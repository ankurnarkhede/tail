const fs = require('mz/fs')
// using mz/fs instead of fs for Promise based response

module.exports = {

  read: function (input_file_path, maxLineCount, encoding) {

    if (encoding == null) {
      encoding = 'utf8'
    }

    const readPrevChar = function (stat, file, currentCharacterCount) {
      return fs.read(file, Buffer.alloc(1), 0, 1,
        stat.size - 1 - currentCharacterCount)
        .then((bytesReadAndBuffer) => {
          return String.fromCharCode(bytesReadAndBuffer[1][0])
        })
    }

    return new Promise((resolve, reject) => {
      let self = {
        stat: null,
        file: null
      }

      fs.exists(input_file_path)
        .then((exists) => {
          if (!exists) {
            console.log('file does not exist...')

            throw new Error('file does not exist...')
          }

        }).then(() => {
        let promises = []

        // Load file Stats.
        promises.push(
          fs.stat(input_file_path)
            .then(stat => self.stat = stat))

        // Open file for reading.
        promises.push(
          fs.open(input_file_path, 'r')
            .then(file => self.file = file))

        return Promise.all(promises)
      }).then(() => {
        let chars = 0
        let lineCount = 0
        let lines = ''

        const do_while = function () {
          if (lines.length > self.stat.size) {
            lines = lines.substring(lines.length - self.stat.size)
          }

          if (lines.length >= self.stat.size || lineCount >= maxLineCount) {
            if (['\n', '\r'].includes(lines.substring(0, 1))) {
              lines = lines.substring(1)
            }
            fs.close(self.file)
            if (encoding === 'buffer') {
              return resolve(Buffer.from(lines, 'binary'))
            }
            return resolve(Buffer.from(lines, 'binary').toString(encoding))
          }

          return readPrevChar(self.stat, self.file, chars)
            .then((nextCharacter) => {
              lines = nextCharacter + lines
              if (['\n', '\r'].includes(nextCharacter) && lines.length > 1) {
                lineCount++
              }
              chars++
            })
            .then(do_while)
        }
        return do_while()

      }).catch((reason) => {
        if (self.file !== null) {
          fs.close(self.file)
        }
        return reject(reason)
      })
    })
  }
}
